import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import kotlin.system.measureTimeMillis

internal class TestMonoAndCoroutineKtTest {

    @Test
    fun `test timeoutOnAsyncAndAwaits_NoAwaitAll_WithMono no exception`() = runBlocking {
        var results: List<Int>
        val time = measureTimeMillis {
            results = timeoutOnAsyncAndAwaits_NoAwaitAll_WithMono(staticTimeOut)
        }
        println("timeoutOnAsyncAndAwaits_NoAwaitAll_WithMono no exception: $time ms")
        assertEquals(listOf(10000, 2, 3, 4, 5), results)
    }

    @Test
    fun `test timeoutOnAsyncAndAwaits_NoAwaitAll_WithMono with exception`() = runBlocking {
        var results: List<Int>
        val time = measureTimeMillis {
            results = timeoutOnAsyncAndAwaits_NoAwaitAll_WithMono(staticTimeOut, true)
        }
        println("timeoutOnAsyncAndAwaits_NoAwaitAll_WithMono with exception: $time ms")
        assertEquals(listOf(10000, 2, 4, 5), results)
    }

    @Test
    fun `test timeoutOnAwaitAndLaunchAwaits_NoAwaitAll_WithMono no exception`() = runBlocking {
        var results: List<Int>
        val time = measureTimeMillis {
            results = timeoutOnAwaitAndLaunchAwaits_NoAwaitAll_WithMono(staticTimeOut)
        }
        println("timeoutOnAwaitAndLaunchAwaits_NoAwaitAll_WithMono no exception: $time ms")
        assertTrue(listOf(10000, 2, 3, 4, 5).containsAll(results))
    }

    @Test
    fun `test timeoutOnAwaitAndLaunchAwaits_NoAwaitAll_WithMono with exception`() = runBlocking {
        var results: List<Int>
        val time = measureTimeMillis {
            results = timeoutOnAwaitAndLaunchAwaits_NoAwaitAll_WithMono(staticTimeOut, true)
        }
        println("timeoutOnAwaitAndLaunchAwaits_NoAwaitAll_WithMono with exception: $time ms")
        assertTrue(listOf(10000, 2, 4, 5).containsAll(results))
    }

    companion object {
        private const val staticTimeOut = 100L
    }
}
