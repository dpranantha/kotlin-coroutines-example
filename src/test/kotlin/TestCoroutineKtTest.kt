import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import kotlin.system.measureTimeMillis

internal class TestCoroutineKtTest {

    @Test
    fun `test timeoutOnAsyncAndAwaits no exception`() = runBlocking {
        var results: List<Int>
        val time = measureTimeMillis {
            results = timeoutOnAsyncAndAwaits(staticTimeOut)
        }
        println("timeoutOnAsyncAndAwaits no exception: $time ms")
        assertEquals(listOf(10000, 2, 3, 4, 5), results) //those are within time limit while the other is cancelled due to timeout exception but taken care of by different coroutines in async
    }

    @Test
    fun `test timeoutOnAsyncAndAwaits with exception`() = runBlocking {
        var results: List<Int>
        val time = measureTimeMillis {
            results = timeoutOnAsyncAndAwaits(staticTimeOut, true)
        }
        println("timeoutOnAsyncAndAwaits with exception: $time ms")
        assertEquals(listOf(10000), results) //awaitAll got cancelled due to exception in one of siblings
    }

    @Test
    fun `test timeoutOnAwaitAndAwaits no exception`() = runBlocking {
        var results: List<Int>
        val time = measureTimeMillis {
            results = timeoutOnAwaitAndAwaits(staticTimeOut)
        }
        println("timeoutOnAwaitAndAwaits no exception: $time ms")
        assertTrue(listOf(10000) == results || listOf(10000, 1, 2, 3, 4, 5) == results) //the awaitAll either they got cancelled due to timeout exception - they are await in the same coroutine
    }

    @Test
    fun `test timeoutOnAwaitAndAwaits with exception`() = runBlocking {
        var results: List<Int>
        val time = measureTimeMillis {
            results = timeoutOnAwaitAndAwaits(staticTimeOut, true)
        }
        println("timeoutOnAwaitAndAwaits with exception: $time ms")
        assertEquals(listOf(10000), results) //awaitAll got cancelled due to exception in one of siblings
    }

    @Test
    fun `test timeoutOnAsyncAndLaunchAwaits no exception`() = runBlocking {
        var results: List<Int>
        val time = measureTimeMillis {
            results = timeoutOnAsyncAndLaunchAwaits(staticTimeOut)
        }
        println("timeoutOnAsyncAndLaunchAwaits no exception: $time ms")
        assertEquals(listOf(10000, 2, 3, 4, 5), results) //those are within time limit while the other is cancelled due to timeout exception but taken care of by different coroutines in async
    }

    @Test
    fun `test timeoutOnAsyncAndLaunchAwaits with exception`() = runBlocking {
        var results: List<Int>
        val time = measureTimeMillis {
            results = timeoutOnAsyncAndLaunchAwaits(staticTimeOut, true)
        }
        println("timeoutOnAsyncAndLaunchAwaits with exception: $time ms")
        assertEquals(listOf(10000), results) //awaitAll got cancelled due to exception in one of siblings
    }

    @Test
    fun `test timeoutOnAwaitAndLaunchAwaits no exception`() = runBlocking {
        var results: List<Int>
        val time = measureTimeMillis {
            results = timeoutOnAwaitAndLaunchAwaits(staticTimeOut)
        }
        println("timeoutOnAwaitAndLaunchAwaits no exception: $time ms")
        assertEquals(listOf(10000), results) //the awaitAll got cancelled due to timeout in one of siblings - they are await in the same coroutine
    }

    @Test
    fun `test timeoutOnAwaitAndLaunchAwaits with exception`() = runBlocking {
        var results: List<Int>
        val time = measureTimeMillis {
            results = timeoutOnAwaitAndLaunchAwaits(staticTimeOut, true)
        }
        println("timeoutOnAwaitAndLaunchAwaits with exception: $time ms")
        assertEquals(listOf(10000), results) //awaitAll got cancelled due to exception in one of siblings
    }

    @Test
    fun `test timeoutOnAsyncAndAwaits_NoAwaitAll no exception`() = runBlocking {
        var results: List<Int>
        val time = measureTimeMillis {
            results = timeoutOnAsyncAndAwaits_NoAwaitAll(staticTimeOut)
        }
        println("timeoutOnAsyncAndAwaits_NoAwaitAll no exception: $time ms")
        assertEquals(listOf(10000, 2, 3, 4, 5), results) //those are within time limit while the other is cancelled due to timeout exception but taken care of by different coroutines in async
    }

    @Test
    fun `test timeoutOnAsyncAndAwaits_NoAwaitAll with exception`() = runBlocking {
        var results: List<Int>
        val time = measureTimeMillis {
            results = timeoutOnAsyncAndAwaits_NoAwaitAll(staticTimeOut, true)
        }
        println("timeoutOnAsyncAndAwaits_NoAwaitAll with exception: $time ms")
        assertEquals(listOf(10000, 2, 4, 5), results) //those are within time limit while the other is cancelled due to timeout and whatever exception but taken care of by different coroutines in async
    }

    @Test
    fun `test timeoutOnAwaitAndAwaits_NoAwaitAll no exception`() = runBlocking {
        var results: List<Int>
        val time = measureTimeMillis {
            results = timeoutOnAwaitAndAwaits_NoAwaitAll(staticTimeOut)
        }
        println("timeoutOnAwaitAndAwaits_NoAwaitAll no exception: $time ms")
        assertEquals(listOf(10000, 2, 3, 4, 5), results) //those are within time limit while the other is cancelled due to timeout exception but taken care of by different coroutines in async
    }

    @Test
    fun `test timeoutOnAwaitAndAwaits_NoAwaitAll with exception`() = runBlocking {
        var results: List<Int>
        val time = measureTimeMillis {
            results = timeoutOnAwaitAndAwaits_NoAwaitAll(staticTimeOut, true)
        }
        println("timeoutOnAwaitAndAwaits_NoAwaitAll with exception: $time ms")
        assertEquals(listOf(10000, 2, 4, 5), results) //those are within time limit while the other is cancelled due to timeout and whatever exception but taken care of by different coroutines in async
    }

    @Test
    fun `test timeoutOnAsyncAndLaunchAwaits_NoAwaitAll no exception`() = runBlocking {
        var results: List<Int>
        val time = measureTimeMillis {
            results = timeoutOnAsyncAndLaunchAwaits_NoAwaitAll(staticTimeOut)
        }
        println("timeoutOnAsyncAndLaunchAwaits_NoAwaitAll no exception: $time ms")
        assertTrue(listOf(10000, 2, 3, 4, 5).containsAll(results)) //those are within time limit while the other is cancelled due to timeout exception but taken care of by different coroutines in async - results unordered
    }

    @Test
    fun `test timeoutOnAsyncAndLaunchAwaits_NoAwaitAll with exception`() = runBlocking {
        var results: List<Int>
        val time = measureTimeMillis {
            results = timeoutOnAsyncAndLaunchAwaits_NoAwaitAll(staticTimeOut, true)
        }
        println("timeoutOnAsyncAndLaunchAwaits_NoAwaitAll with exception: $time ms")
        assertTrue(listOf(10000, 2, 4, 5).containsAll(results)) //those are within time limit while the other is cancelled due to timeout and whatever exception but taken care of by different coroutines in async - results unordered
    }

    @Test
    fun `test timeoutOnAwaitAndLaunchAwaits_NoAwaitAll no exception`() = runBlocking {
        var results: List<Int>
        val time = measureTimeMillis {
            results = timeoutOnAwaitAndLaunchAwaits_NoAwaitAll(staticTimeOut)
        }
        println("timeoutOnAwaitAndLaunchAwaits_NoAwaitAll no exception: $time ms")
        assertTrue(listOf(10000, 2, 3, 4, 5).containsAll(results)) //those are within time limit while the other is cancelled due to timeout exception but taken care of by different coroutines in async - results unordered
    }

    @Test
    fun `test timeoutOnAwaitAndLaunchAwaits_NoAwaitAll with exception`() = runBlocking {
        var results: List<Int>
        val time = measureTimeMillis {
            results = timeoutOnAwaitAndLaunchAwaits_NoAwaitAll(staticTimeOut, true)
        }
        println("timeoutOnAwaitAndLaunchAwaits_NoAwaitAll with exception: $time ms")
        assertTrue(listOf(10000, 2, 4, 5).containsAll(results)) //those are within time limit while the other is cancelled due to timeout and whatever exception but taken care of by different coroutines in async - results unordered
    }

    companion object {
        private const val staticTimeOut = 100L
    }


}
