import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.joinAll
import kotlinx.coroutines.launch
import kotlinx.coroutines.reactive.awaitFirstOrNull
import kotlinx.coroutines.reactive.awaitSingleOrNull
import kotlinx.coroutines.reactor.mono
import kotlinx.coroutines.supervisorScope
import kotlinx.coroutines.withContext
import kotlinx.coroutines.withTimeoutOrNull
import reactor.core.publisher.Mono

suspend fun timeoutOnAsyncAndAwaits_NoAwaitAll_WithMono(timeout: Long, randomException: Boolean = false): List<Int> = withContext(
    Dispatchers.IO) {
    supervisorScope {
        println("supervisorScope thread:  ${Thread.currentThread().name} == runBlocking thread")

        val results = mutableListOf<Int>()

        //100ms limit
        val aNumber = async {
            withTimeoutOrNull(timeout) {
                mono {
                    delay(50)
                    10000
                }.doOnCancel {
                    println("canceled mono aNumber")
                }.doOnError {
                    println("error mono aNumber")
                }.doOnSuccess {
                    println("success mono aNumber")
                }.onErrorResume {
                    Mono.empty()
                }.awaitSingleOrNull()
            }
        }.also {
            println("aNumber thread: ${Thread.currentThread().name}")
        }

        //100ms limit for all calls
        val aSequence = listOf(1, 2, 3, 4, 5).map { value ->
            println("executing Value $value")
            async {
                withTimeoutOrNull(timeout) {
                    mono {
                        if (randomException && value == 3) {
                            throw Exception("whatever")
                        }
                        delay(150L / value)
                        value
                    }.doOnCancel {
                        println("canceled mono sequence")
                    }.doOnError {
                        println("error mono sequence")
                    }.doOnSuccess {
                        println("success mono sequence")
                    }.onErrorResume {
                        Mono.empty()
                    }.awaitSingleOrNull()
                }
            }.also {
                println("Value $value processed by thread: ${Thread.currentThread().name}")
            }
        }

        //subsequent codes will be executed one by one by current thread in suspend - continuation
        //perform await - will block current thread (runblocking)
        val aNumberAwait = try {
            println("aNumberAwait thread:  ${Thread.currentThread().name}  == runBlocking thread")
            aNumber.await()
        } catch (e: Exception) {
            println("Exception: ${e.localizedMessage}")
            null
        }
        if (aNumberAwait != null) results.add(aNumberAwait)

        aSequence.withIndex().forEach {
            val seq = try {
                println("sequence await index ${it.index} thread:  ${Thread.currentThread().name}  == runBlocking thread")
                it.value.await()
            } catch (e: Exception) {
                println("Exception: ${e.localizedMessage}")
                null
            }
            if (seq != null) results.add(seq)
        }
        results
    }
}

suspend fun timeoutOnAwaitAndLaunchAwaits_NoAwaitAll_WithMono (timeout: Long, randomException: Boolean = false): List<Int> = withContext(
    Dispatchers.IO) {
    supervisorScope {
        println("supervisorScope thread:  ${Thread.currentThread().name} == runBlocking thread")

        val results = mutableListOf<Int>()

        val aNumber = async {
            mono {
                delay(50)
                10000
            }.doOnCancel {
                println("canceled mono aNumber")
            }.doOnError {
                println("error mono aNumber")
            }.doOnSuccess {
                println("success mono aNumber")
            }.onErrorResume {
                Mono.empty()
            }.awaitSingleOrNull()
        }.also {
            println("aNumber thread: ${Thread.currentThread().name}")
        }

        val aSequence = listOf(1, 2, 3, 4, 5).map { value ->
            println("executing Value $value")
            async {
                mono {
                    if (randomException && value == 3) {
                        throw Exception("whatever")
                    }
                    delay(150L / value)
                    value
                }.doOnCancel {
                    println("canceled mono sequence")
                }.doOnError {
                    println("error mono sequence")
                }.doOnSuccess {
                    println("success mono sequence")
                }.onErrorResume {
                    Mono.empty()
                }.awaitSingleOrNull()
            }.also {
                println("Value $value processed by thread: ${Thread.currentThread().name}")
            }
        }

        //subsequent codes will be executed by potentially different thread in scheduler IO in suspend continuation fashion
        //perform await - will NOT block current thread (runblocking)
        val job1 = launch {
            val aNumberAwait = withTimeoutOrNull(timeout) {
                try {
                    aNumber.await()
                } catch (e: Exception) {
                    println("Exception: ${e.localizedMessage}")
                    null
                }
            }.also {
                println("aNumberAwait thread:  ${Thread.currentThread().name}  == runBlocking thread")
            }

            if (aNumberAwait != null) results.add(aNumberAwait)
        }

        //awaitAll will be cancelled if one of async call fails OR TIMEOUT
        val job2 = mutableListOf<Job>()
        aSequence.withIndex().forEach {
            val job = launch {
                val seq = withTimeoutOrNull(timeout) {
                    try { //you need to try and catch, otherwise it will just blow on your face
                        it.value.await()
                    } catch (e: Exception) {
                        println("Exception: ${e.localizedMessage}")
                        null
                    }
                }.also {
                    println("aSequenceAwaitAll thread:  ${Thread.currentThread().name}  == runBlocking thread")
                }
                if (seq != null) results.add(seq)
            }
            job2.add(job)
        }

        job1.join()
        job2.joinAll()
        results
    }
}
