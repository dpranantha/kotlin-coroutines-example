import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.supervisorScope
import kotlinx.coroutines.withContext
import kotlinx.coroutines.withTimeoutOrNull

suspend fun timeoutOnAsyncAndAwaits(timeout: Long, randomException: Boolean = false): List<Int> = withContext(
    Dispatchers.IO) {
    supervisorScope {
        println("supervisorScope thread:  ${Thread.currentThread().name} == runBlocking thread")

        val results = mutableListOf<Int>()

        //100ms limit
        val aNumber = async {
            withTimeoutOrNull(timeout) {
                delay(50)
                10000
            }
        }.also {
            println("aNumber thread: ${Thread.currentThread().name}")
        }

        //100ms limit for all calls
        val aSequence = listOf(1, 2, 3, 4, 5).map { value ->
            println("executing Value $value")
            async {
                withTimeoutOrNull(timeout) {
                    if (randomException && value == 3) {
                        throw Exception("whatever")
                    }
                    delay(150L / value)
                    value
                }
            }.also {
                println("Value $value processed by thread: ${Thread.currentThread().name}")
            }
        }

        //subsequent codes will be executed one by one by current thread in suspend - continuation
        //perform await - will block current thread (runblocking)
        val aNumberAwait = try {
            println("aNumberAwait thread:  ${Thread.currentThread().name}  == runBlocking thread")
            aNumber.await()
        } catch (e: Exception) {
            println("Exception: ${e.localizedMessage}")
            null
        }
        if (aNumberAwait != null) results.add(aNumberAwait)

        //awaitAll will be cancelled if one of async call fails
        val aSequenceAwaitAll = try {
            println("aSequenceAwaitAll thread:  ${Thread.currentThread().name}  == runBlocking thread")
            aSequence.awaitAll()
        } catch (e: Exception) {
            println("Exception: ${e.localizedMessage}")
            emptyList<Int>()
        }
        results.addAll(aSequenceAwaitAll.filterNotNull())

        results
    }
}

suspend fun timeoutOnAwaitAndAwaits(timeout: Long, randomException: Boolean = false): List<Int> = withContext(
    Dispatchers.IO) {
    supervisorScope {
        println("supervisorScope thread:  ${Thread.currentThread().name} == runBlocking thread")

        val results = mutableListOf<Int>()

        val aNumber = async {
            delay(50)
            10000
        }.also {
            println("aNumber thread: ${Thread.currentThread().name}")
        }

        val aSequence = listOf(1, 2, 3, 4, 5).map { value ->
            println("executing Value $value")
            async {
                if (randomException && value == 3) {
                    throw Exception("whatever")
                }
                delay(150L / value)
                value
            }.also {
                println("Value $value processed by thread: ${Thread.currentThread().name}")
            }
        }

        //subsequent codes will be executed one by one by current thread in suspend - continuation
        //perform await - will block current thread (runblocking)
        val aNumberAwait = withTimeoutOrNull(timeout) {
            try {
                aNumber.await()
            } catch (e: Exception) {
                println("Exception: ${e.localizedMessage}")
                null
            }
        }.also {
            println("aNumberAwait thread:  ${Thread.currentThread().name}  == runBlocking thread")
        }

        if (aNumberAwait != null) results.add(aNumberAwait)

        //awaitAll will be cancelled if one of async call fails
        val aSequenceAwaitAll =  withTimeoutOrNull(timeout) {
            try { //you need to try and catch, otherwise it will just blow on your face
                aSequence.awaitAll()
            } catch (e: Exception) {
                println("Exception: ${e.localizedMessage}")
                emptyList()
            }
        }.also {
            println("aSequenceAwaitAll thread:  ${Thread.currentThread().name}  == runBlocking thread")
        }
        results.addAll(aSequenceAwaitAll.orEmpty())

        results
    }
}

suspend fun timeoutOnAsyncAndLaunchAwaits(timeout: Long, randomException: Boolean = false): List<Int> = withContext(
    Dispatchers.IO) {
    supervisorScope {
        println("supervisorScope thread:  ${Thread.currentThread().name} == runBlocking thread")

        val results = mutableListOf<Int>()

        //100ms limit
        val aNumber = async {
            withTimeoutOrNull(timeout) {
                delay(50)
                10000
            }
        }.also {
            println("aNumber thread: ${Thread.currentThread().name}")
        }

        //100ms limit for all calls
        val aSequence = listOf(1, 2, 3, 4, 5).map { value ->
            println("executing Value $value")
            async {
                withTimeoutOrNull(timeout) {
                    if (randomException && value == 3) {
                        throw Exception("whatever")
                    }
                    delay(150L / value)
                    value
                }
            }.also {
                println("Value $value processed by thread: ${Thread.currentThread().name}")
            }
        }

        //subsequent codes will be executed by potentially different thread in scheduler IO in suspend continuation fashion
        //perform await - will NOT block current thread (runblocking)
        launch {
            val aNumberAwait = try {
                println("aNumberAwait thread:  ${Thread.currentThread().name}  == runBlocking thread")
                aNumber.await()
            } catch (e: Exception) {
                println("Exception: ${e.localizedMessage}")
                null
            }
            if (aNumberAwait != null) results.add(aNumberAwait)
        }

        //awaitAll will be cancelled if one of async call fails
        launch {
            val aSequenceAwaitAll = try {
                println("aSequenceAwaitAll thread:  ${Thread.currentThread().name}  == runBlocking thread")
                aSequence.awaitAll()
            } catch (e: Exception) {
                println("Exception: ${e.localizedMessage}")
                emptyList<Int>()
            }
            results.addAll(aSequenceAwaitAll.filterNotNull())
        }

        results
    }
}

suspend fun timeoutOnAwaitAndLaunchAwaits(timeout: Long, randomException: Boolean = false): List<Int> = withContext(
    Dispatchers.IO) {
    supervisorScope {
        println("supervisorScope thread:  ${Thread.currentThread().name} == runBlocking thread")

        val results = mutableListOf<Int>()

        val aNumber = async {
            delay(50)
            10000
        }.also {
            println("aNumber thread: ${Thread.currentThread().name}")
        }

        val aSequence = listOf(1, 2, 3, 4, 5).map { value ->
            println("executing Value $value")
            async {
                if (randomException && value == 3) {
                    throw Exception("whatever")
                }
                delay(150L / value)
                value
            }.also {
                println("Value $value processed by thread: ${Thread.currentThread().name}")
            }
        }

        //subsequent codes will be executed by potentially different thread in scheduler IO in suspend continuation fashion
        //perform await - will NOT block current thread (runblocking)
        launch {
            val aNumberAwait = withTimeoutOrNull(timeout) {
                try {
                    aNumber.await()
                } catch (e: Exception) {
                    println("Exception: ${e.localizedMessage}")
                    null
                }
            }.also {
                println("aNumberAwait thread:  ${Thread.currentThread().name}  == runBlocking thread")
            }

            if (aNumberAwait != null) results.add(aNumberAwait)
        }

        //awaitAll will be cancelled if one of async call fails OR TIMEOUT
        launch {
            val aSequenceAwaitAll = withTimeoutOrNull(timeout) {
                try { //you need to try and catch, otherwise it will just blow on your face
                    aSequence.awaitAll()
                } catch (e: Exception) {
                    println("Exception: ${e.localizedMessage}")
                    emptyList()
                }
            }.also {
                println("aSequenceAwaitAll thread:  ${Thread.currentThread().name}  == runBlocking thread")
            }
            results.addAll(aSequenceAwaitAll?.filterNotNull().orEmpty())
        }

        results
    }
}

suspend fun timeoutOnAsyncAndAwaits_NoAwaitAll(timeout: Long, randomException: Boolean = false): List<Int> = withContext(
    Dispatchers.IO) {
    supervisorScope {
        println("supervisorScope thread:  ${Thread.currentThread().name} == runBlocking thread")

        val results = mutableListOf<Int>()

        //100ms limit
        val aNumber = async {
            withTimeoutOrNull(timeout) {
                delay(50)
                10000
            }
        }.also {
            println("aNumber thread: ${Thread.currentThread().name}")
        }

        //100ms limit for all calls
        val aSequence = listOf(1, 2, 3, 4, 5).map { value ->
            println("executing Value $value")
            async {
                withTimeoutOrNull(timeout) {
                    if (randomException && value == 3) {
                        throw Exception("whatever")
                    }
                    delay(150L / value)
                    value
                }
            }.also {
                println("Value $value processed by thread: ${Thread.currentThread().name}")
            }
        }

        //subsequent codes will be executed one by one by current thread in suspend - continuation
        //perform await - will block current thread (runblocking)
        val aNumberAwait = try {
            println("aNumberAwait thread:  ${Thread.currentThread().name}  == runBlocking thread")
            aNumber.await()
        } catch (e: Exception) {
            println("Exception: ${e.localizedMessage}")
            null
        }
        if (aNumberAwait != null) results.add(aNumberAwait)

        aSequence.withIndex().forEach {
            val seq = try {
                println("sequence await index ${it.index} thread:  ${Thread.currentThread().name}  == runBlocking thread")
                it.value.await()
            } catch (e: Exception) {
                println("Exception: ${e.localizedMessage}")
                null
            }
            if (seq != null) results.add(seq)
        }
        results
    }
}


suspend fun timeoutOnAwaitAndAwaits_NoAwaitAll(timeout: Long, randomException: Boolean = false): List<Int> = withContext(
    Dispatchers.IO) {
    supervisorScope {
        println("supervisorScope thread:  ${Thread.currentThread().name} == runBlocking thread")

        val results = mutableListOf<Int>()

        val aNumber = async {
            delay(50)
            10000
        }.also {
            println("aNumber thread: ${Thread.currentThread().name}")
        }

        val aSequence = listOf(1, 2, 3, 4, 5).map { value ->
            println("executing Value $value")
            async {
                if (randomException && value == 3) {
                    throw Exception("whatever")
                }
                delay(150L / value)
                value
            }.also {
                println("Value $value processed by thread: ${Thread.currentThread().name}")
            }
        }

        //subsequent codes will be executed one by one by current thread in suspend - continuation
        //perform await - will block current thread (runblocking)
        val aNumberAwait = withTimeoutOrNull(timeout) {
            try {
                aNumber.await()
            } catch (e: Exception) {
                println("Exception: ${e.localizedMessage}")
                null
            }
        }.also {
            println("aNumberAwait thread:  ${Thread.currentThread().name}  == runBlocking thread")
        }

        if (aNumberAwait != null) results.add(aNumberAwait)

        aSequence.withIndex().forEach {
            val seq = withTimeoutOrNull(timeout) {
                try { //you need to try and catch, otherwise it will just blow on your face
                    it.value.await()
                } catch (e: Exception) {
                    println("Exception: ${e.localizedMessage}")
                    null
                }
            }.also {
                println("aSequenceAwaitAll thread:  ${Thread.currentThread().name}  == runBlocking thread")
            }
            if (seq != null) results.add(seq)
        }
        results
    }
}

suspend fun timeoutOnAsyncAndLaunchAwaits_NoAwaitAll(timeout: Long, randomException: Boolean = false): List<Int> = withContext(
    Dispatchers.IO) {
    supervisorScope {
        println("supervisorScope thread:  ${Thread.currentThread().name} == runBlocking thread")

        val results = mutableListOf<Int>()

        //100ms limit
        val aNumber = async {
            withTimeoutOrNull(timeout) {
                delay(50)
                10000
            }
        }.also {
            println("aNumber thread: ${Thread.currentThread().name}")
        }

        //100ms limit for all calls
        val aSequence = listOf(1, 2, 3, 4, 5).map { value ->
            println("executing Value $value")
            async {
                withTimeoutOrNull(timeout) {
                    if (randomException && value == 3) {
                        throw Exception("whatever")
                    }
                    delay(150L / value)
                    value
                }
            }.also {
                println("Value $value processed by thread: ${Thread.currentThread().name}")
            }
        }

        //subsequent codes will be executed by potentially different thread in scheduler IO in suspend continuation fashion - results are randomly appended due to multiple workers (threads) / unordered
        //perform await - will NOT block current thread (runblocking)
        launch {
            val aNumberAwait = try {
                println("aNumberAwait thread:  ${Thread.currentThread().name}  == runBlocking thread")
                aNumber.await()
            } catch (e: Exception) {
                println("Exception: ${e.localizedMessage}")
                null
            }
            if (aNumberAwait != null) results.add(aNumberAwait)
        }

        aSequence.withIndex().forEach {
            launch {
                val seq = try {
                    println("sequence await index ${it.index} thread:  ${Thread.currentThread().name}  == runBlocking thread")
                    it.value.await()
                } catch (e: Exception) {
                    println("Exception: ${e.localizedMessage}")
                    null
                }
                if (seq != null) results.add(seq)
            }
        }

        results
    }
}

suspend fun timeoutOnAwaitAndLaunchAwaits_NoAwaitAll(timeout: Long, randomException: Boolean = false): List<Int> = withContext(
    Dispatchers.IO) {
    supervisorScope {
        println("supervisorScope thread:  ${Thread.currentThread().name} == runBlocking thread")

        val results = mutableListOf<Int>()

        val aNumber = async {
            delay(50)
            10000
        }.also {
            println("aNumber thread: ${Thread.currentThread().name}")
        }

        val aSequence = listOf(1, 2, 3, 4, 5).map { value ->
            println("executing Value $value")
            async {
                if (randomException && value == 3) {
                    throw Exception("whatever")
                }
                delay(150L / value)
                value
            }.also {
                println("Value $value processed by thread: ${Thread.currentThread().name}")
            }
        }

        //subsequent codes will be executed by potentially different thread in scheduler IO in suspend continuation fashion
        //perform await - will NOT block current thread (runblocking)
        launch {
            val aNumberAwait = withTimeoutOrNull(timeout) {
                try {
                    aNumber.await()
                } catch (e: Exception) {
                    println("Exception: ${e.localizedMessage}")
                    null
                }
            }.also {
                println("aNumberAwait thread:  ${Thread.currentThread().name}  == runBlocking thread")
            }

            if (aNumberAwait != null) results.add(aNumberAwait)
        }

        //awaitAll will be cancelled if one of async call fails OR TIMEOUT
        aSequence.withIndex().forEach {
            launch {
                val seq = withTimeoutOrNull(timeout) {
                    try { //you need to try and catch, otherwise it will just blow on your face
                        it.value.await()
                    } catch (e: Exception) {
                        println("Exception: ${e.localizedMessage}")
                        null
                    }
                }.also {
                    println("aSequenceAwaitAll thread:  ${Thread.currentThread().name}  == runBlocking thread")
                }
                if (seq != null) results.add(seq)
            }
        }

        results
    }
}
